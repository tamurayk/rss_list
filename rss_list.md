### web-news RSSリスト

* [スラッシュドット・ジャパン: IT](https://srad.jp/it.rss)
* [スラッシュドット・ジャパン: Linux](https://srad.jp/linux.rss)
* [スラッシュドット・ジャパン: オープンソース](https://srad.jp/opensource.rss)
* [スラッシュドット・ジャパン: セキュリティ](https://srad.jp/security.rss)
* [Qiita - 人気の投稿](http://qiita.com/popular-items/feed)
* [はてなブックマーク - 人気エントリー - テクノロジー](http://b.hatena.ne.jp/hotentry/it.rss)
* [Amazon Web Services ブログ](http://feeds.feedburner.com/AmazonWebServicesBlogJp)
* [Mackerel ブログ #mackerelio](http://blog-ja.mackerel.io/rss)
* [Planet PHP](http://www.planet-php.net/rdf/)
* [PHPDeveloper.org](http://www.phpdeveloper.org/feed)
